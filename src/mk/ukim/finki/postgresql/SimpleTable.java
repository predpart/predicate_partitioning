package mk.ukim.finki.postgresql;

public class SimpleTable {

	public String schemaname;
	public String tablename;
	
	public SimpleTable(){
		this.schemaname = null;
		this.tablename = null;
	}
	
	public SimpleTable(String schemaname, String tablename) {
		this.schemaname = schemaname;
		this.tablename = tablename;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((schemaname == null) ? 0 : schemaname.hashCode());
		result = prime * result
				+ ((tablename == null) ? 0 : tablename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTable other = (SimpleTable) obj;
		if (schemaname == null) {
			if (other.schemaname != null)
				return false;
		} else if (!schemaname.equals(other.schemaname))
			return false;
		if (tablename == null) {
			if (other.tablename != null)
				return false;
		} else if (!tablename.equals(other.tablename))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.toTextTable();
		//return "{" + this.schemaname + "," + this.tablename + "}";
	}
	
	public String toTextTable() {
		return String.format("%s.%s", schemaname, tablename);
	}
}

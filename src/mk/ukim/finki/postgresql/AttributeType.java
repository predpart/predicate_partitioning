package mk.ukim.finki.postgresql;

public enum AttributeType {
	
	NUMERIC, TEXT, DATE;
	
}

package mk.ukim.finki.postgresql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class PredicateBitmaps {
	
	final static Map<AtomicPredicate, HugeBitArray> predicateBitArrays = new HashMap<AtomicPredicate, HugeBitArray>();
	
	// predicates: all predicates that were extracted from the workload
	public static void generatePredicateBitArrays(AtomicPredicate[] predicates) throws ClassNotFoundException, SQLException {

		for(AtomicPredicate p : predicates) {
			 SimpleTable t = new SimpleTable("public", p.table);
			 if(p.schema != null) {
				 if(!p.schema.isEmpty())
					 t.schemaname = p.schema;
			 }
			 long nBits = TableMetadata.rows.get(t);
			 HugeBitArray bitArray = new HugeBitArray(nBits);
			 String sql = "select rownum - 1 from (select row_number() over(order by " + getPrimaryKeyColumns(t) + ") as rownum, " + p.attribute + " from " + t + ") x where " + p.toTextPredicateRaw();
			 System.out.println(sql);
			 ResultSet rs = PostgresDBService.executeQuery(sql);
			 long bitIndex;
			 while(rs.next()) {
				 bitIndex = Long.valueOf(rs.getString(1));
				 bitArray.setBit(bitIndex);
			 }
			 rs.close();
			 predicateBitArrays.put(p, bitArray);
			 
			 // do the same for the reverted predicate
			 AtomicPredicate pInverse = AtomicPredicate.inverse(p);
			 HugeBitArray inverseBitArray = new HugeBitArray(bitArray, true);
			 predicateBitArrays.put(pInverse, inverseBitArray);
		}
	}
	
	// predicates: predicates over the same table
	public static long selectCountAll(List<AtomicPredicate> predicates) {
		
		SimpleTable t = new SimpleTable("public", predicates.get(0).table);
		if(predicates.get(0).schema != null) {
			if(!predicates.get(0).schema.isEmpty())
				t.schemaname = predicates.get(0).schema;
		}
		long nBits = TableMetadata.rows.get(t);
		if(predicates.size() == 1) {
			System.out.println("For predicates on " + t +" " + predicates +", the cardinality is " + predicateBitArrays.get(predicates.get(0)).bitCount());
			return predicateBitArrays.get(predicates.get(0)).bitCount();
		}
		else {
			HugeBitArray intersection = new HugeBitArray(predicateBitArrays.get(predicates.get(0)), false);			
			for(int i = 1; i < predicates.size(); i++) {
				intersection = intersection.bitwiseAnd(predicateBitArrays.get(predicates.get(i)));
			}
			System.out.println("For predicates on " + t +" " + predicates +", the cardinality is " + intersection.bitCount());
			return intersection.bitCount();
		}		
	
	}
	
	private static String getPrimaryKeyColumns(SimpleTable table) {
		return "(SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type" +
			   "FROM   pg_index i" +
			   "JOIN   pg_attribute a ON a.attrelid = i.indrelid" +
			   "                     AND a.attnum = ANY(i.indkey)" +
			   "WHERE  i.indrelid = '" + table.tablename + "'::regclass" +
			   "AND    i.indisprimary)";

	}
}

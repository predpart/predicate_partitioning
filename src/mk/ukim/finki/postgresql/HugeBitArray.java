package mk.ukim.finki.postgresql;
import java.util.Random;

public class HugeBitArray {
	public long size;
	public long[] a;
	private int leftoverBits;	// the number of leftover bits in the last 64-bit long number due to fixed 64-bit length (64 * a.length - size)
	
	public HugeBitArray(long size) {
		this.size = size;
		int s = (int) Math.ceil((double) size / 64.0);
		this.a = new long[s];
		this.leftoverBits = (int) ((long) (64 * this.a.length) - this.size);
	}
	
	public HugeBitArray(HugeBitArray rhs, boolean invert) {
		this.size = rhs.size;
		this.a = new long[rhs.a.length];
		this.leftoverBits = (int) ((long) (64 * this.a.length) - this.size);
		if(invert) {
            for (int i = 0; i < rhs.a.length; i++) {
                this.a[i] = ~rhs.a[i];
            }
            // invert back the leftover bits (they must remain zero)
            for (int i = 0; i < this.leftoverBits; i++) {
                this.a[this.a.length - 1] &= ~(1L << (63 - i));		// highest bits in the last long number
            }
        }
		else {
			for(int i = 0; i < rhs.a.length; i++) {
				this.a[i] = rhs.a[i];
			}
		}
	}

	public void invert() {
		for(int i = 0; i < this.a.length; i++) {
			this.a[i] = ~this.a[i];
		}
		
		// invert back the leftover bits (they must remain zero)
		for(int i = 0; i < this.leftoverBits; i++) {
			this.a[this.a.length - 1] &= ~(1L << (63 - i));		// highest bits in the last long number
		}
	}
	
	public void initRandomValues() {
		Random r = new Random();
		for(int i =0; i < a.length; i++) {
			a[i] = r.nextLong();
		}
	}
	
	public void initOnes() {
		for(int i = 0; i < a.length; i++) {
			a[i] = Long.MAX_VALUE | 1L << 63;	// explicitly set the highest bit (the longs are signed)
		}
		
	}	
	
	public void setBit(long position) {
		int aIndex = (int) position / 64;
		int bitInLong = (int) position % 64; // no need for 63 - position % 64
		a[aIndex] |= 1L << bitInLong;
	}
	
	public boolean checkBit(long position) {	// returns true if the bit is 1
		int aIndex = (int) position / 64;
		int bitInLong = (int) position % 64;
		return (a[aIndex] & 1L << bitInLong) == (1L << bitInLong);
	}
	
	// moze da se smeni da nema rezultat, tuku vo ovoj objekt da bide rezultatot
	public HugeBitArray bitwiseAnd(final HugeBitArray rhs) {
		assert this.size == rhs.size;

		final HugeBitArray res = new HugeBitArray(this.size);
		for(int i = 0; i < a.length; i++) {
			res.a[i] = a[i] & rhs.a[i];
		}
		return res;
	}
	
	public long bitCount() {
		
		long cnt = 0;
		for(int i = 0; i < a.length; i++) {
			cnt += Long.bitCount(a[i]);
		}
		return cnt;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.a.length; i++) {
			String binary = Long.toBinaryString(this.a[i]);
			if(binary.length() < 64) {
				for(int j = 0; j < 64 - binary.length(); j++) {
					sb.append('0');
				}
			}
			sb.append(binary);
		}
		return sb.toString();
	}
}
package mk.ukim.finki.postgresql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public final class TableMetadata {
	
	final static Map<SimpleTable, Long> rows = new HashMap<SimpleTable, Long>();
	
	
	
	public static void countRows() throws ClassNotFoundException, SQLException {
		for(SimpleTable t : TableMapping.tableAttributes.keySet()) {
			String sql = "SELECT COUNT(*) FROM " + t;
			ResultSet rs = PostgresDBService.executeQuery(sql);
			rs.next();
			long rowCount = Long.valueOf(rs.getString(1));
			rs.close();
			rows.put(t, rowCount);
		}
	}
	
	
	
}

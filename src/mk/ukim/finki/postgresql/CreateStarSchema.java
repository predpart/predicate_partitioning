package mk.ukim.finki.postgresql;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class CreateStarSchema {

	private static final String DDL_DELIMITER = ";";
	private static final String DATA_DELIMITER = "|";
	private static final String PATH_TO_DDL_SCRIPT = "ssb.ddl";
	private static final String[] PATHS_TO_DATA = new String[] {
		"/home/nino/ssb_data/part.tbl",
		"/home/nino/ssb_data/supplier.tbl",
		"/home/nino/ssb_data/customer.tbl",
		"/home/nino/ssb_data/date.tbl",
		"/home/nino/ssb_data/lineorder_.tbl",
	};
	private static final String[] TABLE_NAMES = new String[] {
		"part",
		"supplier",
		"customer",
		"dates",
		"lineorder",
	};

	private static void loadTable(String table, String path) throws SQLException, ClassNotFoundException {
		PostgresDBService.executeUpdate(
				"COPY " + table + " FROM '" + path + "' (DELIMITER '" + DATA_DELIMITER +"');");
	}
	
	public static void createDatabase(String databaseName, String owner, String username, String password)
			throws ClassNotFoundException, SQLException, IOException {		
		PostgresDBService.getConnection("postgres", username, password);
		Scanner s = null;
		String script;
		boolean isDatabaseDefault = databaseName.toLowerCase().equals("postgres");
		try{
			s = new Scanner(new File(PATH_TO_DDL_SCRIPT));
			script = s.useDelimiter("\\Z").next();
		} finally {
			s.close();
		}
		
		if(!script.isEmpty()) {
			try{
				if(!isDatabaseDefault)
				PostgresDBService.executeUpdate(String.format(
						"CREATE DATABASE \"%s\" WITH OWNER = %s", databaseName, username));
			} catch(Exception e) {
				PostgresDBService.closeStatement();
				PostgresDBService.closeConnection();
				System.err.println("Database " + databaseName + "could not be created!");
				System.exit(1);
			} finally {
				if(!isDatabaseDefault) {
					PostgresDBService.closeStatement();
					PostgresDBService.closeConnection();
				}
			}
			
			for(String table : TABLE_NAMES) {
				PostgresDBService.executeUpdate("DROP TABLE IF EXISTS " + table + " CASCADE");
			}
			
			if(!isDatabaseDefault)
				PostgresDBService.getConnection(databaseName, username, password);
			PostgresDBService.executeSqlScript(script, DDL_DELIMITER);
			
			for(int i = 0; i < TABLE_NAMES.length; i++) {
				try {
					loadTable(TABLE_NAMES[i], PATHS_TO_DATA[i]);
				} catch (Exception e) {
					System.out.println(e);
					System.err.println(String.format("Data could not be loaded to table %s from file %s!", TABLE_NAMES[i], PATHS_TO_DATA[i]));
				}
			}		
		}		
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		String databaseName = "postgres";
		String username = "postgres";
		String password = "postgres";
		createDatabase(databaseName, "postgres", username, password);
		System.out.println("Database created.");
	}
}

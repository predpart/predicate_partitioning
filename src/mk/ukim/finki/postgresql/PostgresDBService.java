package mk.ukim.finki.postgresql;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PSQLException;


public class PostgresDBService {

	private static Connection connection = null;
	private static ResultSet resultSet = null;
	private static Statement statement = null;
	
	public static boolean isConnectionActive() throws SQLException {
		return (connection != null && !connection.isClosed());
	}

	public static Connection getConnection(String databaseName, String username,
			String password) throws SQLException, ClassNotFoundException {
		if (connection != null && (!connection.isClosed())){
			return connection;
		}
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/" + databaseName, username, password);
		connection.setAutoCommit(true);
		return connection;
	}
	
	public static void closeStatement() throws SQLException {
		try{}
		finally {
			if(statement != null) statement.close();
		}
	}
	
	public static void closeConnection() throws SQLException {
		try{}
		finally {
			if(connection != null) connection.close();
		}
	}
	
	public static void executeUpdate(String stmt) throws SQLException, ClassNotFoundException {
		
		if(statement == null && connection != null && !connection.isClosed()) {
			statement = connection.createStatement();
		}
		try{
			statement.executeUpdate(stmt);
		} catch(PSQLException pe) {
			System.err.println("PSQL Exception caught successfully!");
			getConnection("postgres", "postgres", "postgres");
			connection.createStatement();
			statement.executeUpdate(stmt);
		}
	}
	
	public static ResultSet executeQuery(String stmt) throws SQLException, ClassNotFoundException {
		if(statement == null && connection != null && !connection.isClosed()) {
			statement = connection.createStatement();
		}
		try {
			resultSet = statement.executeQuery(stmt);
		} catch (PSQLException pe) {
			System.err.println("PSQL Exception caught successfully!");
			getConnection("postgres", "postgres", "postgres");
			connection.createStatement();
            resultSet = statement.executeQuery(stmt);
		}
		return resultSet;
	}

	public static PreparedStatement getPreparedStatement(String stmt, Object... args) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(stmt);
		int i = 1;
		for (Object arg : args) {
			if (arg instanceof Byte) {
				ps.setByte(i++, (Byte) arg); //used to represent a bit
			} else if (arg instanceof Short) {
				ps.setShort(i++, (Short) arg);
			} else if (arg instanceof Integer) {
				ps.setInt(i++, (Integer) arg);
			} else if (arg instanceof Long) {
				ps.setLong(i++, (Long) arg);
			} else if (arg instanceof BigDecimal) {
				ps.setBigDecimal(i++, (BigDecimal) arg);
			} else if (arg instanceof String) {
				ps.setString(i++, (String) arg);
			} else if (arg instanceof Float) {
				ps.setFloat(i++, (Float) arg);
			} else if (arg instanceof Double) {
				ps.setDouble(i++, (Double) arg);
			} else if (arg instanceof byte[]) {
				ps.setBytes(i++, (byte[]) arg);
			} else if (arg instanceof Boolean) {
				ps.setBoolean(i++, (boolean) arg);
			} else if (arg instanceof java.sql.Date) {
				ps.setDate(i++, (java.sql.Date) arg);
			} else if (arg instanceof java.sql.Time) {
				ps.setTime(i++, (java.sql.Time) arg);
			} else if (arg instanceof java.sql.Timestamp) {
				ps.setTimestamp(i++, (java.sql.Timestamp) arg);
			} else if (arg instanceof java.sql.Blob) {
				ps.setBlob(i++, (java.sql.Blob) arg);
			} else if (arg instanceof java.sql.Clob) {
				ps.setClob(i++, (java.sql.Clob) arg);
			} else if (arg instanceof java.sql.Array) {
				ps.setArray(i++, (java.sql.Array) arg);
			} else if (arg instanceof java.sql.Ref) {
				ps.setRef(i++, (java.sql.Ref) arg);
			} 
		}
		return ps;
	}
	
	public static void executeUpdateParameterized(PreparedStatement ps) throws SQLException {
		ps.executeUpdate();
		resultSet = null;
	}
	
	public static ResultSet executeQueryParameterized(PreparedStatement ps) throws SQLException {
		resultSet = ps.executeQuery();
		return resultSet;
	}
	
	public static List<String> getStatementsFromSqlScript(String script, String statementDelimiter) {
		String[] lines = script.split("\\r?\\n");
		List<String> stmts = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		for(String line : lines) {
			String trimmedLine = line.trim();
			if(!trimmedLine.isEmpty()) {
				if(!trimmedLine.startsWith("--") && !trimmedLine.startsWith("/") && !trimmedLine.startsWith("*")){
					sb.append(trimmedLine);
					sb.append("\n");
					if(trimmedLine.endsWith(statementDelimiter)) {
						stmts.add(sb.toString());
						sb = new StringBuilder();
					}
				}
			}			
		}
		return stmts;
	}
	
	public static ArrayList<ResultSet> executeSqlScript(String script, String statementDelimiter) throws SQLException, ClassNotFoundException {
		ArrayList<ResultSet> results = new ArrayList<ResultSet>();
		List<String> statements = getStatementsFromSqlScript(script, statementDelimiter);
		for(String statement : statements) {
			if(statement.toLowerCase().contains("select") || statement.toLowerCase().contains("explain")) {
				ResultSet rs = executeQuery(statement);
				results.add(rs);
			} else {
				executeUpdate(statement);
				results.add(null);
			}
		}
		return results;
	}
	
	public static void vacuum() throws SQLException, ClassNotFoundException {
		String sql = "VACUUM FULL ANALYZE;";
		executeUpdate(sql);
	}
	
	public ResultSet explainQuery(String stmt, Object...args) throws SQLException {
		stmt = "EXPLAIN (FORMAT JSON) " + stmt;
		PreparedStatement ps = getPreparedStatement(stmt, args);
		return executeQueryParameterized(ps);
	}
	
	public static ExplainResult parseExplainResults(ResultSet results, PreparedStatement ps) throws SQLException, JSONException {
		ExplainResult er = null;
		if (results.next()) {
			
			String res = results.getString(1);
			//System.out.println(res);
			//System.out.println("ESTIMATION EXECUTION PLAN");
			//System.out.println(res);
			JSONArray arr = new JSONArray(res);
			JSONObject rootPlan = arr.getJSONObject(0).getJSONObject("Plan");
			double executionCost = rootPlan.getDouble("Total Cost");
			int rowsEstimation = rootPlan.getInt("Plan Rows");
			er = new ExplainResult(executionCost, rowsEstimation);			
		}
		ps.close();
		return er;	
	}	
	
	public static double getCost(String stmt, boolean simulated, Object...args) throws SQLException, JSONException {
		if(simulated) {
			stmt = "EXPLAIN (FORMAT JSON) " + stmt;
			PreparedStatement ps = getPreparedStatement(stmt, args);
			ExplainResult res = parseExplainResults(executeQueryParameterized(ps), ps);
			return res.executionCost;
		} else {
			stmt = "EXPLAIN (FORMAT JSON) " + stmt;
			PreparedStatement ps = getPreparedStatement(stmt, args);
			ExplainResult res = parseAnalyzeResults(executeQueryParameterized(ps), ps);
			return res.executionCost;
		}
		
	} 
	
	public static ExplainResult parseAnalyzeResults(ResultSet results, PreparedStatement ps) throws SQLException, JSONException {
		ExplainResult er = null;
		if (results.next()) {
			//System.out.println("ACTUAL EXPLAIN:");
			
			String res = results.getString(1);
			//System.out.println("ACTUAL EXECUTION PLAN: ");
			//System.out.println(res);
			//System.out.println(res);
			/*int j = res.indexOf("..");
			int k = res.indexOf(' ', j);
			String costString = res.substring(j + 2, k);
			double cost = Double.valueOf(costString);
			er = new ExplainResult(cost, 0);*/
			JSONArray arr = new JSONArray(res);
			JSONObject rootPlan = arr.getJSONObject(0).getJSONObject("Plan");
			double executionCost = rootPlan.getDouble("Total Cost");
			int rowsEstimation = rootPlan.getInt("Plan Rows");
			er = new ExplainResult(executionCost, rowsEstimation);
		}
		ps.close();
		return er;
	}
}

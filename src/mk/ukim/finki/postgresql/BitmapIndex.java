package mk.ukim.finki.postgresql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class BitmapIndex {
	
	public HashMap<String, HugeBitArray> matrix;	// HugeBitArrays correspond to different values of the attribute
									// each bit array encodes the rows in the table for the specific attribute value
	private SimpleTable table;
	private String attribute;
	
	public BitmapIndex() {	}
	
	public BitmapIndex(SimpleTable table, String attribute, AtomicPredicate predicate) {
		if(!table.tablename.equals(predicate.table))
			throw new IllegalArgumentException("Predicate table must match the table name provided in the first argument");
		// da se fati i ako ima da se pusti obicno query (koga table != p.table). ako p.table e null i atributot e od taa tabela, togas se setira table vo predicatot
		// p.table i table mora da se od neparticioniranata schema
		
		this.table = table;
		this.attribute = attribute;
		this.matrix = new HashMap<String, HugeBitArray>();
		int position = getAttributePosition();	// get the position in the sorted list
		for(String distinctValue : PredicateBitmapsDistinct.universe.get(table).get(attribute).keySet()) {
			if(PredicateBitmapsDistinct.bitArrays.get(predicate).get(position).	// check if the value is in a row that satisfies the predicate
					checkBit(PredicateBitmapsDistinct.universe.get(table).get(attribute).get(distinctValue))) {
				// create a bit array with length of the number of rows in the table
				HugeBitArray bitmap = new HugeBitArray(TableMetadata.rows.get(table));
				//String sql = ""
				this.matrix.put(distinctValue, bitmap);
				
				// now, create an ordinary bitmap for this value 
				
			}
		}
		
	}
	
	private int getAttributePosition() {
		// find the position of the attribute in the sorted attribute list
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.addAll(TableMapping.tableAttributes.get(this.table));
		Collections.sort(attributes);
		int pos = -1;
		for(int i = 0; i < attributes.size(); i++) {
			if(attributes.get(i).equals(this.attribute)) {
				pos = i;
				break;
			}
 		}
		return pos;
	}
	
	
}

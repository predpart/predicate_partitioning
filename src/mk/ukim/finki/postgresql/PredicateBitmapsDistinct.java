package mk.ukim.finki.postgresql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.SynchronousQueue;

public final class PredicateBitmapsDistinct {
	
	public static final Map<AtomicPredicate, ArrayList<HugeBitArray>> bitArrays = new HashMap<AtomicPredicate, ArrayList<HugeBitArray>>();
	public static final Map<SimpleTable, TreeMap<String, Long>> nBits = new HashMap<SimpleTable, TreeMap<String, Long>>();
	// table -> {attribute->{distinct value -> position in bit array}}
	public static final Map<SimpleTable, TreeMap<String, TreeMap<String, Long>>> universe = new HashMap<SimpleTable, TreeMap<String, TreeMap<String, Long>>>();
	
	
	// tables: tables from the non-partitioned schema
	public static void generateUniverse() throws ClassNotFoundException, SQLException {
		for(SimpleTable t : TableMapping.tableAttributes.keySet()) {
			universe.put(t, new TreeMap<String, TreeMap<String, Long>>());
			nBits.put(t, new TreeMap<String, Long>());
			long nBitsForAttribute;
			for(String att : TableMapping.tableAttributes.get(t)) {
				universe.get(t).put(att, new TreeMap<String, Long>());
				String sql = "SELECT DISTINCT " + att + " FROM " + t + " ORDER BY " + att + " ASC;";
				ResultSet rs = PostgresDBService.executeQuery(sql);
				String value;
				long bitIndex = 0;
				while(rs.next()) {
					value = rs.getString(1);
					universe.get(t).get(att).put(value, bitIndex);
					bitIndex++;
				}	
				rs.close();
				nBitsForAttribute = bitIndex;
				nBits.get(t).put(att, nBitsForAttribute);
			}
			System.out.println("Universe for " + t + " generated.");
		}
	}
	
	// predicates: all predicates extracted from the workload
	public static void generateBitArrays(AtomicPredicate[] extractedPredicates) throws ClassNotFoundException, SQLException {
		
		for(AtomicPredicate p : extractedPredicates) {
			ArrayList<HugeBitArray> bitArrayList = new ArrayList<HugeBitArray>();
			ArrayList<HugeBitArray> bitArrayListInverse = new ArrayList<HugeBitArray>();
			SimpleTable predicateTable = new SimpleTable(p.schema, p.table);
			AtomicPredicate pInverse = AtomicPredicate.inverse(p);

			
			for(SimpleTable t : TableMapping.tableAttributes.keySet()) {
				if(t.tablename.equals(p.table) && TableMapping.tableAttributes.get(t).contains(p.attribute)) {
					predicateTable = t;
					break;
				}
			}
			
			
			HashSet<String> attSet = TableMapping.tableAttributes.get(predicateTable);
			TreeSet<String> attSetSorted = new TreeSet<String>();	// required to preserve order
			attSetSorted.addAll(attSet);
			
			int attIndex = 0;
			for(String att : attSetSorted) {
				String sql = "SELECT DISTINCT " + att + " FROM " + predicateTable + " WHERE " + p.toTextPredicateWithoutSchema();
				ResultSet rs = PostgresDBService.executeQuery(sql);
				String value;
				HugeBitArray bitArray = new HugeBitArray(nBits.get(predicateTable).get(att));
				int cnt = 0;
				TreeSet<String> tss = new TreeSet<String>(); 
				while(rs.next()) {
					value = rs.getString(1);
					long bitIndex = universe.get(predicateTable).get(att).get(value);
					bitArray.setBit(bitIndex);cnt++;tss.add(value);
				}
				rs.close();
				bitArrayList.add(attIndex, bitArray);
				
				String sqlInverse = "SELECT DISTINCT " + att + " FROM " + predicateTable + " WHERE " + pInverse.toTextPredicateWithoutSchema();
				ResultSet rsInverse = PostgresDBService.executeQuery(sqlInverse);
				HugeBitArray bitArrayInverse = new HugeBitArray(nBits.get(predicateTable).get(att));
				cnt = 0;
				TreeSet<String> ts = new TreeSet<String>();
				while(rsInverse.next()) {
					value = rsInverse.getString(1);
					long bitIndex = universe.get(predicateTable).get(att).get(value);
					bitArrayInverse.setBit(bitIndex);cnt++;ts.add(value);
				}
				rsInverse.close();
				bitArrayListInverse.add(attIndex++, bitArrayInverse);
			}
			
			bitArrays.put(p, bitArrayList);
			bitArrays.put(pInverse, bitArrayListInverse);	// Do the same for the inverted predicate pInverse
			// Each list for every predicate over the same table has its attribute order preserved
		}
	}
	
	// predicates: a list of predicates over a table
	public static Map<String, Long> selectCountDistinctAllAttributes(List<AtomicPredicate> predicates) {
		System.out.println("Calculating count distinct for predicates " + predicates);
		int nAttributes;
		long[] Nbits;
		TreeSet<String> attSetSorted = new TreeSet<String>();	// we need this to preserve order of indexing (i-th attribute must match)
		SimpleTable t;
		if(predicates.get(0).schema != null) {
			t = new SimpleTable(predicates.get(0).schema, predicates.get(0).table);
		} else {
			t=  new SimpleTable("public", predicates.get(0).table);
		}
		nAttributes = TableMapping.tableAttributes.get(t).size();
		attSetSorted.addAll(TableMapping.tableAttributes.get(t));
		Nbits = new long[nAttributes];
		int nBitsIndex = 0;
		for(String att : attSetSorted) {
			Nbits[nBitsIndex++] = nBits.get(t).get(att);
		}
		
		// an intersection for each attribute of the table
		// intersection[i] is the intersection of the i-th attribute for all predicates over the table
		HugeBitArray[] intersection = new HugeBitArray[nAttributes];
		for(int i = 0; i < intersection.length; i++) {
			intersection[i] = new HugeBitArray(Nbits[i]);
			intersection[i].initOnes();    // we need this for AND-ing in a loop
		}
		
		for(AtomicPredicate p : predicates) {
			for(int i = 0; i < nAttributes; i++) {
				HugeBitArray cp = new HugeBitArray(intersection[i], false);
				intersection[i] = intersection[i].bitwiseAnd(bitArrays.get(p).get(i));
			}
		}
		
		long[] countDistinct = new long[nAttributes];
		for(int i = 0; i < nAttributes; i++) {
			countDistinct[i] = intersection[i].bitCount();
		}
		
		Map<String, Long> countDistinctMap = new TreeMap<String, Long>();
		int attIndex = 0;
		for(String att : attSetSorted) {	// iterate in ascending order by attribute name
			countDistinctMap.put(att, countDistinct[attIndex++]);		// countDistinct[] is organized in ascending order by attribute name
		}
		
		return countDistinctMap;		
	}
	
}

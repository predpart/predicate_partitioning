package jenes.implementation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import jenes.GenerationEventListener;
import jenes.GeneticAlgorithm;
import jenes.chromosome.BooleanChromosome;
import jenes.population.Fitness;
import jenes.population.Individual;
import jenes.population.Population;
import jenes.population.Population.Statistics;
import jenes.population.Population.Statistics.Group;
import jenes.stage.AbstractStage;
import jenes.stage.operator.common.OnePointCrossover;
import jenes.stage.operator.common.SimpleMutator;
import jenes.stage.operator.common.TournamentSelector;
import jenes.tutorials.utils.Utils;
import mk.ukim.finki.postgresql.AtomicPredicate;
import mk.ukim.finki.postgresql.Constants;
import mk.ukim.finki.postgresql.PartitioningUtils;
import mk.ukim.finki.postgresql.PostgresDBService;
import mk.ukim.finki.postgresql.PredicateBitmaps;
import mk.ukim.finki.postgresql.PredicateBitmapsDistinct;
import mk.ukim.finki.postgresql.SimpleTable;
import mk.ukim.finki.postgresql.SystemCatalogManager;
import mk.ukim.finki.postgresql.TableMetadata;
import mk.ukim.finki.postgresql.WorkloadPredicates;
import mk.ukim.finki.postgresql.WorkloadQuery;

import org.json.JSONException;

public class JenesGAControlled {
	
	// Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates SODRZI NULL VREDNOSTI VO VALUE AKO ZA KLUCOT NE POSTOJAT
	// PREDICATES VO WORKLOAD

	private  int POPULATION_SIZE = 20;
	private  int GENERATION_LIMIT = 100;
	private double globalMin;
	private  String[] m_tables;
	private  File f = new File("res_controlled_P20_G100.txt");

	
	@SuppressWarnings("unused")
	public  Map<SimpleTable, Set<AtomicPredicate>> createNewPartitionedSchema(
			String schemaName, String newSchemaName, String[] oldTables,
			List<AtomicPredicate> selectedPredicates)
			throws ClassNotFoundException, FileNotFoundException, SQLException,
			JSONException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, InstantiationException {

	
		Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
		SimpleTable[] starr = new SimpleTable[oldTables.length];
		for (int i = 0; i < oldTables.length; i++) {
			starr[i] = new SimpleTable(schemaName, oldTables[i]);
		}
		PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS "
				+ newSchemaName + " CASCADE");
		// not really needed
		PostgresDBService.executeUpdate("VACUUM");
		partitionsAndTheirPredicates = PartitioningUtils.createPartitionedSchema(newSchemaName, selectedPredicates);
		//for(SimpleTable stt : partitionsAndTheirPredicates.keySet()){
		//	System.out.println(stt + " = {");
		//	for(AtomicPredicate app : partitionsAndTheirPredicates.get(stt)) {
		//		System.out.println("  " + app);
		//	}
		//	System.out.println("}");
		//}
		List<SimpleTable> desiredPartitions = populateCatalogEntries_allpartitions(starr, partitionsAndTheirPredicates);
		//printPgStats(desiredPartitions);
		return partitionsAndTheirPredicates;
	}
	
	public  Map<SimpleTable, Set<AtomicPredicate>> 
	createEmptyPartitionedSchema(
			String schemaName, String newSchemaName, String[] oldTables,
			List<AtomicPredicate> selectedPredicates) throws SQLException, FileNotFoundException, ClassNotFoundException{
		
		// Create the new empty partitions
		Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
		SimpleTable[] starr = new SimpleTable[oldTables.length];
		for (int i = 0; i < oldTables.length; i++) {
			starr[i] = new SimpleTable(schemaName, oldTables[i]);
		}
		PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS " + newSchemaName + " CASCADE;");
		//not really needed
		PostgresDBService.executeUpdate("VACUUM;");
		partitionsAndTheirPredicates = PartitioningUtils.createPartitionedSchema(newSchemaName, selectedPredicates);
		//for(SimpleTable stt : partitionsAndTheirPredicates.keySet()){
		//	System.out.println(stt + " = {");
		//	for(AtomicPredicate app : partitionsAndTheirPredicates.get(stt)) {
		//		System.out.println("  " + app);
		//	}
		//	System.out.println("}");
		//}
		return partitionsAndTheirPredicates;
	}
	
	public  List<SimpleTable> insertDataIntoPartitionedSchema(
			String originalSchemaName, String partitionedSchemaName,
			Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates) throws SQLException, ClassNotFoundException {
		// PartitionsAndTheirPredicates contains pairs (partition, predicates that generate(constrain) 'partition')
		List<SimpleTable> populatedPartitions = new ArrayList<SimpleTable>();
		//System.out.println("PARTITIONS AND THEIR PREDICATES");
		//System.out.println(partitionsAndTheirPredicates);
		for (SimpleTable st : partitionsAndTheirPredicates.keySet()) {
			if(partitionsAndTheirPredicates.get(st) != null) {
				if(!st.tablename.contains("_master")) {
					String st_name = st.tablename;
					int j = st_name.length() - 1;
					Set<AtomicPredicate> originalTablePredicates = new HashSet<>();
					
					while(st_name.charAt(j--) != '_');
					st_name = st_name.substring(0, j+1);
					
					
					for(AtomicPredicate ap : partitionsAndTheirPredicates.get(st)) {
						AtomicPredicate ap_orig = new AtomicPredicate(originalSchemaName, st_name, ap.attribute, ap.attributeType, ap.operator, ap.value);
						originalTablePredicates.add(ap_orig);
					}
					
					StringBuilder sb = new StringBuilder();
					for(AtomicPredicate ap : originalTablePredicates) {
						sb.append(ap.toTextPredicateWithoutSchema());
						sb.append(" AND ");
					}
					String predicateConjunction = sb.toString();
					
					String insertSQL = "INSERT INTO " + st.toTextTable()
								   + " (SELECT * FROM " + originalSchemaName + "." + st_name
								   + " WHERE " + predicateConjunction.substring(0, predicateConjunction.length() - 5) + ");";
					populatedPartitions.add(st);
					PostgresDBService.executeUpdate(insertSQL);
					//PostgresDBService.executeUpdate("analyze " + st);
					
				}
			} else {
				// no predicates for this table, so we have a single partiton -- just copy the data
				// we need a more intelligent way of getting the original table name as it may contain an undescore ( _ )
				if(!st.tablename.contains("_master")) {
					String insertCopySQL = "INSERT INTO " + st.toTextTable()
									   + " (SELECT * FROM " + originalSchemaName + "." + st.tablename.substring(0, st.tablename.indexOf('_')) + ");";
					populatedPartitions.add(st);
					PostgresDBService.executeUpdate(insertCopySQL);
					
					//dodadeno
					//PostgresDBService.executeUpdate("analyze " + st);
				}
			}
			
			
		}
		PostgresDBService.executeUpdate("analyze;");
		return populatedPartitions;
	}

	public List<SimpleTable> populateCatalogEntries_allpartitions(
			SimpleTable[] oldTables,
			Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates)
			throws ClassNotFoundException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, InstantiationException,
			SQLException {
		String newschema = null;
		boolean newschemaSet = false;
		List<SimpleTable> partitionsOfPredicates = new ArrayList<SimpleTable>();
		for (int i = 0; i < oldTables.length; i++) {
			SystemCatalogManager catalogManager = new SystemCatalogManager(
					oldTables[i].schemaname, oldTables[i].tablename);
			catalogManager.retrieveMasterTableCatalogData();
			for (SimpleTable partition : partitionsAndTheirPredicates.keySet()) {
				if (partition.tablename.startsWith(oldTables[i].tablename + "_")) {
					if (!newschemaSet) {
						newschema = partition.schemaname;
						newschemaSet = true;
					}
					if (partitionsAndTheirPredicates.get(partition) == null)
						catalogManager.copyStatisticsForPseudoPartition(partition);
					else {
						catalogManager.updateStatisticsForPartition(partition, partitionsAndTheirPredicates.get(partition));
						partitionsOfPredicates.add(partition);
					}
				}
			}
			if (newschema != null) {
				
				// Master tables should be empty, but they need to contain statistics
				// for their children
				// We do this by avoiding the pg_class update!
				
				SimpleTable master = new SimpleTable(newschema,
						oldTables[i].tablename + "_master");
				catalogManager.copyStatisticsForMasterTable(master);
				
			}
		}
		
		return partitionsOfPredicates;
	}
	
	public void printPgStats(List<SimpleTable> desiredPartitions) throws SQLException, ClassNotFoundException {
		for(SimpleTable p : desiredPartitions) {
			String tablename = p.tablename;
			String sql = "select * from pg_stats where tablename = '" + tablename + "' order by attname;";
			ResultSet rs = PostgresDBService.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			for(int j = 1; j <=rsmd.getColumnCount(); j++)
				System.out.printf("%-32s", rsmd.getColumnName(j));
			System.out.println();
			while(rs.next()) {
				
				for(int j=1; j<=rsmd.getColumnCount(); j++) {
					System.out.printf("%-32s",rs.getString(j));
				}
				System.out.println();
			}
			rs.close();
		}
		System.out.println();
	}

	public  void initConnection(String db, String user, String pass)
			throws ClassNotFoundException, SQLException {
		PostgresDBService.getConnection(db, user, pass);
	}

	public  void closeConnection() throws SQLException {
		PostgresDBService.closeStatement();
		PostgresDBService.closeConnection();
	}
	
	public  void setAllPredicatesArray() throws JSONException {
		String[] workload = Constants.WORKLOAD.split(";");
		HashSet<AtomicPredicate> uniquePredicates = new HashSet<>();
		for(String query : workload) {
			WorkloadQuery q = new WorkloadQuery(query);
			q.extractPredicates();
			List<AtomicPredicate> q_predicates = q.getPredicates();
			for(AtomicPredicate p : q_predicates) {
				uniquePredicates.add(p);
			}
		}
		// da se otstranat duplikati
		WorkloadPredicates.predicates = new AtomicPredicate[uniquePredicates.size()];
		int i = 0;
		for(AtomicPredicate p : uniquePredicates) {
			WorkloadPredicates.predicates[i] = p;
			i++;
		}
	}
	
	private Map<String, Double> estimatedToActual = new HashMap<>();

	public  void runga() throws IOException, ClassNotFoundException, SQLException {
		
		
		
		// Initialize row counts for each table in the non-partitioned schema
		TableMetadata.countRows();		
		// Initialize bit arrays (distinct) for each inidividual predicate extracted from 
		// the workload
		PredicateBitmapsDistinct.generateUniverse();
		System.out.println("Universe generated.");
		PredicateBitmapsDistinct.generateBitArrays(WorkloadPredicates.predicates);
		System.out.println("Distinct bit arrays generated.");
		// Initialize basic bit arrays for countAll		
		PredicateBitmaps.generatePredicateBitArrays(WorkloadPredicates.predicates);
		System.out.println("Regular bit arrays generated.");
		
		
		
		
		
		final PrintWriter pw = new PrintWriter(f);
		pw.println("generation,min_fitness,mean_fitness,global_min");
		pw.close();
		final File cost_debug = new File("cost_tracking.txt");
		final PrintWriter pww = new PrintWriter(cost_debug);
		pww.println("estimated,actual,error(%)");
		pww.close();
		globalMin = Double.POSITIVE_INFINITY;
		m_tables = new String[] { "customer", "part", "supplier", "dates", "lineorder" };
		Individual<BooleanChromosome> sample = new Individual<BooleanChromosome>(
				new BooleanChromosome(WorkloadPredicates.predicates.length));
		Population<BooleanChromosome> pop = new Population<BooleanChromosome>(sample, POPULATION_SIZE);
		

		Fitness<BooleanChromosome> fit = new Fitness<BooleanChromosome>(1, false) {
			@SuppressWarnings("unused")
			@Override
			public void evaluate(Individual<BooleanChromosome> individual) {
				long start = System.currentTimeMillis();
				BooleanChromosome chrom = individual.getChromosome();
				List<AtomicPredicate> selectedPredicates = new ArrayList<>();
				for (int i = 0; i < chrom.length(); i++) {
					//int val = chrom.getValue(i) ? 1:0;
		
					if (chrom.getValue(i))
						selectedPredicates.add(WorkloadPredicates.predicates[i]);
				}
				
				double totalCost = 0.0;
				if(selectedPredicates.isEmpty()) {
					individual.setScore(totalCost);
					individual.setLegal(false);
					return;
				}
				
				
				
				//System.out.println("Selected predicates: [");
				//for(AtomicPredicate p : selectedPredicates) {
				//	System.out.println(p);
				//}
				//System.out.println("]");
				Map<SimpleTable, Set<AtomicPredicate>> partitionedSchema = null;
				try {
					PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS partitioned CASCADE;");
					//PostgresDBService.executeUpdate("VACUUM;");
					partitionedSchema = createNewPartitionedSchema("public", "partitioned", m_tables, selectedPredicates);
				} catch (FileNotFoundException | SQLException
						| ClassNotFoundException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException
						| InstantiationException | JSONException e) {
					System.err.println("The solution could not be evaluated. Stopping genetic algorithm.");
					e.printStackTrace();
					System.exit(1);
				}
				
				//try {
					//PostgresDBService.executeUpdate(
					//		"Copy (select * from pg_stats where schemaname='partitioned' order by tablename, attname) to '/home/nino/Desktop/partitioned_stats.csv' With CSV;");
					//PostgresDBService.executeUpdate(
					//		"Copy (select * from pg_class where relname in (select tablename from pg_stats where schemaname='partitioned')) to '/home/nino/Desktop/partitioned_pg_class.csv' With CSV;");
				
				//} catch (SQLException e1) {
					// TODO Auto-generated catch block
				//	e1.printStackTrace();
				//} catch (ClassNotFoundException e) {
				//	// TODO Auto-generated catch block
				//	e.printStackTrace();
				//}
				

				String[] queries = mk.ukim.finki.postgresql.Constants.WORKLOAD.split(";");
				for (int i = 0; i < queries.length; i++) {
					WorkloadQuery q = new WorkloadQuery(queries[i]);
					
					try {
						//System.out.println("Executing query: ");
						//System.out.println(q.getSql());
						q.extractPredicates();
						totalCost += q.calculateExecutionCost(partitionedSchema, true);
						//System.out.println(queries[i]);
						//System.out.println(totalCost);
						//System.out.println("------------------------------------------------");
					} catch (JSONException | SQLException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}
				StringBuilder est_act_cost = new StringBuilder();
				est_act_cost.append(totalCost);
				System.out.println("CHROMOSOME COST = " + totalCost);
				long end = System.currentTimeMillis();
				System.out.println("ELAPSED TIME: " + (end - start) + " ms.");
				System.out.println("TOTAL ELAPSED TIME FOR THE COMPLEX GROUPING QUERY: " + Constants.TOTAL_Q_TIME +" ms.");
				
				// We do not allow an execution cost of 0.0
				
				individual.setScore(totalCost);
				if(totalCost == 0.0) {
					individual.setLegal(false);
				} else {
					individual.setLegal(true);
					if(individual.getScore() < globalMin)
						globalMin = individual.getScore();
				}
				
				
				//String statsdebug = "copy (select * from pg_stats where schemaname='partitioned' order by tablename, attname) to '/home/nino/Desktop/partitioned_stats.csv' delimiter '*' csv header;";
				//try {
				//	PostgresDBService.executeUpdate(statsdebug);
				//} catch (SQLException e1) {
					// TODO Auto-generated catch block
					//e1.printStackTrace();
				//}
				//Scanner s = new Scanner(new InputStreamReader(System.in));
				//System.out.println("LINE READ 1");
				//s.nextLine();	
			
				// Re-create the partitioned schema, but this time insert real data into partitions
				
				start = System.currentTimeMillis();
				try {
					PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS partitioned CASCADE;");
					//PostgresDBService.executeUpdate("VACUUM;");
					partitionedSchema = createEmptyPartitionedSchema("public", "partitioned", m_tables, selectedPredicates);
					List<SimpleTable> desiredPartitions = insertDataIntoPartitionedSchema("public", "partitioned", partitionedSchema);
					PostgresDBService.executeUpdate("ANALYZE;");
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//try {
					//PostgresDBService.executeUpdate(
					//		"Copy (select * from pg_stats where schemaname='partitioned' order by tablename, attname) to '/home/nino/Desktop/public_stats.csv' With CSV;");
					//PostgresDBService.executeUpdate(
					//		"Copy (select * from pg_class where relname in (select tablename from pg_stats where schemaname='partitioned')) to '/home/nino/Desktop/public_pg_class.csv' With CSV;");
				
				//} catch (SQLException | ClassNotFoundException e1) {
					// TODO Auto-generated catch block
				//	e1.printStackTrace();
				//}
								
				totalCost = 0.0;
				for (int i = 0; i < queries.length; i++) {
					WorkloadQuery q = new WorkloadQuery(queries[i]);					
					try {
						q.extractPredicates();
						//System.out.println("Executing query: ");
						//System.out.println(q.getSql());
						totalCost += q.calculateExecutionCost(partitionedSchema, false);
					//	System.out.println(queries[i]);
					//	System.out.println(totalCost);
					//	System.out.println("------------------------------------------------");
					} catch (JSONException | SQLException e){
						System.out.println("Error in actual data evaluation!");
						e.printStackTrace();
						System.exit(1);
					}
				}
			
				end = System.currentTimeMillis();
				
				
				
				estimatedToActual.put(individual.getChromosome().toString(), totalCost);
				est_act_cost.append("," + totalCost);
				try(PrintWriter ppp = new PrintWriter(new BufferedWriter(new FileWriter(cost_debug, true)))) {
					double est = Double.parseDouble(est_act_cost.toString().split(",")[0]);
					double act = Double.parseDouble(est_act_cost.toString().split(",")[1]);
					double err;
					if(est <= act) {
						err = -((act - est) / act) * 100;
					}
					else {
						err = ((est - act) / act * 100);
					}
					est_act_cost.append("," + err);
					ppp.println(est_act_cost.toString());
					ppp.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("(DATA) CHROMOSOME COST = " + totalCost);
				System.out.println("(DATA) ELAPSED TIME: " + (end-start) + " ms.");
				//Scanner s2 = new Scanner(new InputStreamReader(System.in));
				//System.out.println("LINE READ 2");
				//s2.nextLine();
				
			}			
		};
		
		GenerationEventListener<BooleanChromosome> generationEventListener = new GenerationEventListener<BooleanChromosome>() {
			@Override
			public void onGeneration(GeneticAlgorithm ga, long time) {
				Statistics stat = ga.getLastPopulation().getStatistics();
				int generation = ga.getGeneration();
				Group legals = stat.getGroup(Population.LEGALS);
				double min_score = legals.getMin()[0];
				double mean_score = legals.getMean()[0];
				double actual_cost = estimatedToActual.get(stat.getLegalLowestIndividual().getChromosome().toString());
				//double actualCost = estimatedToActual.get(fittestIndividualForGen);
				try(PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter(f, true)))) {
					p.println(generation + "," + min_score + "," + mean_score + "," + actual_cost + "," + globalMin);
					p.close();
				} catch (IOException e) {
					System.err.println("Could not write to results file!");
					e.printStackTrace();
				}
				
			}
		};

		GeneticAlgorithm<BooleanChromosome> ga = new GeneticAlgorithm<BooleanChromosome>(fit, pop, GENERATION_LIMIT);
	
		AbstractStage<BooleanChromosome> selection = new TournamentSelector<BooleanChromosome>(3);
		AbstractStage<BooleanChromosome> crossover = new OnePointCrossover<BooleanChromosome>(0.8);
		AbstractStage<BooleanChromosome> mutation = new SimpleMutator<BooleanChromosome>(0.02);
		ga.addStage(selection);
		ga.addStage(crossover);
		ga.addStage(mutation);		
		ga.addGenerationEventListener(generationEventListener);

		ga.setElitism(1); 	

		ga.evolve();

		Population.Statistics stats = ga.getLastPopulation().getStatistics();
		GeneticAlgorithm.Statistics algostats = ga.getStatistics();

		System.out.println("Objective: " + (fit.getBiggerIsBetter()[0] ? "Max! (All true)" : "Min! (none true)"));
		System.out.println();

		Group legals = stats.getGroup(Population.LEGALS);
		Individual solution = legals.get(0);

		System.out.println("Solution: ");
		System.out.println(solution);
		System.out.format("found in %d ms.\n", algostats.getExecutionTime());
		System.out.println();
		
		try(PrintWriter pp = new PrintWriter(new BufferedWriter(new FileWriter(f, true)))) {
			pp.println();
			pp.println("Lowest score found with evolution up to last generation:");
			pp.println(solution);
			pp.println("Score: " + solution.getScore());
			pp.close();
		}

		Utils.printStatistics(stats);
	}	
}

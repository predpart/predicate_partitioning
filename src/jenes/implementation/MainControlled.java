package jenes.implementation;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import mk.ukim.finki.postgresql.AtomicPredicate;
import mk.ukim.finki.postgresql.AttributeType;
import mk.ukim.finki.postgresql.Operator;

import org.json.JSONException;

public class MainControlled {

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException, JSONException, IOException {
	
		long start = System.currentTimeMillis();
		JenesGAControlled jenesGA = new JenesGAControlled();
		jenesGA.setAllPredicatesArray();
		jenesGA.initConnection("postgres", "postgres", "postgres");
		jenesGA.runga();
		jenesGA.closeConnection();
		long end = System.currentTimeMillis();
		long secs = (end - start) / 1000;
		System.out.println(secs + "s elapsed since the start.");
		
	}
} 

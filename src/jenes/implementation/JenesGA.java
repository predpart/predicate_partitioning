package jenes.implementation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jenes.GenerationEventListener;
import jenes.GeneticAlgorithm;
import jenes.chromosome.BooleanChromosome;
import jenes.population.Fitness;
import jenes.population.Individual;
import jenes.population.Population;
import jenes.population.Population.Statistics;
import jenes.population.Population.Statistics.Group;
import jenes.stage.AbstractStage;
import jenes.stage.operator.common.OnePointCrossover;
import jenes.stage.operator.common.SimpleMutator;
import jenes.stage.operator.common.TournamentSelector;
import jenes.tutorials.utils.Utils;
import mk.ukim.finki.postgresql.AtomicPredicate;
import mk.ukim.finki.postgresql.Constants;
import mk.ukim.finki.postgresql.PartitioningUtils;
import mk.ukim.finki.postgresql.PostgresDBService;
import mk.ukim.finki.postgresql.SimpleTable;
import mk.ukim.finki.postgresql.SystemCatalogManager;
import mk.ukim.finki.postgresql.WorkloadPredicates;
import mk.ukim.finki.postgresql.WorkloadQuery;

import org.json.JSONException;

public class JenesGA {
	 
	// Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates SODRZI NULL VREDNOSTI VO VALUE AKO ZA KLUCOT NE POSTOJAT
	// PREDICATES VO WORKLOAD

	private  int POPULATION_SIZE = 20;
	private  int GENERATION_LIMIT = 100;
	private double globalMin;
	private  String[] m_tables;
	private  double[] actualCosts = new double[GENERATION_LIMIT];
	private  int chromosomeCount = 0, generationCount = 0;
	private  File f = new File("/home/nino/Research/PredicatePartitioning2014/results_14_jan/evolution_results_pop20_gen100_uncontrolled_2GBram");

	
	public  Map<SimpleTable, Set<AtomicPredicate>> createNewPartitionedSchema(
			String schemaName, String newSchemaName, String[] oldTables,
			List<AtomicPredicate> selectedPredicates)
			throws ClassNotFoundException, FileNotFoundException, SQLException,
			JSONException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, InstantiationException {

		Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
		SimpleTable[] starr = new SimpleTable[oldTables.length];
		for (int i = 0; i < oldTables.length; i++) {
			starr[i] = new SimpleTable(schemaName, oldTables[i]);
		}
		PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS "
				+ newSchemaName + " CASCADE");
		PostgresDBService.executeUpdate("vacuum");
		partitionsAndTheirPredicates = PartitioningUtils
				.createPartitionedSchema(newSchemaName, selectedPredicates);
		populateCatalogEntries_allpartitions(starr,
				partitionsAndTheirPredicates);
		return partitionsAndTheirPredicates;
	}
	
	public  Map<SimpleTable, Set<AtomicPredicate>> 
	createEmptyPartitionedSchema(
			String schemaName, String newSchemaName, String[] oldTables,
			List<AtomicPredicate> selectedPredicates) throws SQLException, FileNotFoundException, ClassNotFoundException{
		
		// Create the new empty partitions
		Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
		SimpleTable[] starr = new SimpleTable[oldTables.length];
		for (int i = 0; i < oldTables.length; i++) {
			starr[i] = new SimpleTable(schemaName, oldTables[i]);
		}
		PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS "
				+ newSchemaName + " CASCADE;");
		PostgresDBService.executeUpdate("VACUUM;");
		partitionsAndTheirPredicates = PartitioningUtils.createPartitionedSchema(newSchemaName, selectedPredicates);
		
		return partitionsAndTheirPredicates;
	}
	
	public  void insertDataIntoPartitionedSchema(
			String originalSchemaName, String partitionedSchemaName,
			Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates) throws SQLException, ClassNotFoundException {
		//System.out.println(partitionsAndTheirPredicates );
		// PartitionsAndTheirPredicates contains pairs (partition, predicates that generate(constrain) 'partition')
		for (SimpleTable st : partitionsAndTheirPredicates.keySet()) {
			if(partitionsAndTheirPredicates.get(st) != null) {
			//System.out.println("TABELA IMEEEEEEEE: " + st.toTextTable());
			String st_name = st.tablename;
			int j = st_name.length() - 1;
			Set<AtomicPredicate> originalTablePredicates = new HashSet<>();
			
			while(st_name.charAt(j--) != '_');
			st_name = st_name.substring(0, j+1);
			
			
			for(AtomicPredicate ap : partitionsAndTheirPredicates.get(st)) {
				AtomicPredicate ap_orig = new AtomicPredicate(originalSchemaName, st_name, ap.attribute, ap.attributeType, ap.operator, ap.value);
				originalTablePredicates.add(ap_orig);
			}
			
			StringBuilder sb = new StringBuilder();
			for(AtomicPredicate ap : originalTablePredicates) {
				sb.append(ap.toTextPredicateWithoutSchema());
				sb.append(" AND ");
			}
			String predicateConjunction = sb.toString();
			
			String insertSQL = "INSERT INTO " + st.toTextTable()
						   + " (SELECT * FROM " + originalSchemaName + "." + st_name
						   + " WHERE " + predicateConjunction.substring(0, predicateConjunction.length() - 5) + ");";
			//System.out.println(insertSQL);
			PostgresDBService.executeUpdate(insertSQL);
			}
			
		}
	}

	public  void populateCatalogEntries_allpartitions(
			SimpleTable[] oldTables,
			Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates)
			throws ClassNotFoundException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, InstantiationException,
			SQLException {
		String newschema = null;
		boolean newschemaSet = false;
		for (int i = 0; i < oldTables.length; i++) {
			SystemCatalogManager catalogManager = new SystemCatalogManager(
					oldTables[i].schemaname, oldTables[i].tablename);
			catalogManager.retrieveMasterTableCatalogData();
			for (SimpleTable partition : partitionsAndTheirPredicates.keySet()) {
				if (partition.tablename
						.startsWith(oldTables[i].tablename + "_")) {
					if (!newschemaSet)
						newschema = partition.schemaname;
					newschemaSet = true;
					if (partitionsAndTheirPredicates.get(partition) == null)
						catalogManager
								.copyStatisticsForPseudoPartition(partition);
					else
						catalogManager.updateStatisticsForPartition(partition,
								partitionsAndTheirPredicates.get(partition));
				}
			}
			if (newschema != null) {
				SimpleTable master = new SimpleTable(newschema,
						oldTables[i].tablename + "_master");
				catalogManager.copyStatisticsForPseudoPartition(master);
			}
		}
		//String sql = "select * from pg_stats where schemaname='partitioned' and (tablename='lineorder_0' or tablename = 'lineorder_0_test');";
		//ResultSet rs = PostgresDBService.executeQuery(sql);
		//ResultSetMetaData rsmd = rs.getMetaData();
		//while(rs.next()) {
		//	for(int j=1; j<=rsmd.getColumnCount(); j++) {
		//		System.out.print(rs.getString(j) + "\t");
		//	}
		//	System.out.println();
		//}
		//rs.close();
	}

	public  void initConnection(String db, String user, String pass)
			throws ClassNotFoundException, SQLException {
		PostgresDBService.getConnection(db, user, pass);
	}

	public  void closeConnection() throws SQLException {
		PostgresDBService.closeStatement();
		PostgresDBService.closeConnection();
	}
	
	public  void setAllPredicatesArray() throws JSONException {
		String[] workload = Constants.WORKLOAD.split(";");
		HashSet<AtomicPredicate> uniquePredicates = new HashSet<>();
		for(String query : workload) {
			WorkloadQuery q = new WorkloadQuery(query);
			q.extractPredicates();
			List<AtomicPredicate> q_predicates = q.getPredicates();
			for(AtomicPredicate p : q_predicates) {
				uniquePredicates.add(p);
			}
		}
		
		WorkloadPredicates.predicates = new AtomicPredicate[uniquePredicates.size()];
		int i = 0;
		for(AtomicPredicate p : uniquePredicates) {
			WorkloadPredicates.predicates[i] = p;
			i++;
		}
	}
	
	private Map<String, Double> estimatedToActual = new HashMap<>();

	public  void runga() throws IOException {
		
		final PrintWriter pw = new PrintWriter(f);
		pw.println("generation,min_fitness,mean_fitness,global_min");
		pw.close();
		globalMin = Double.POSITIVE_INFINITY;
		m_tables = new String[] { "customer", "part", "supplier", "dates", "lineorder" };
		Individual<BooleanChromosome> sample = new Individual<BooleanChromosome>(
				new BooleanChromosome(WorkloadPredicates.predicates.length));
		Population<BooleanChromosome> pop = new Population<BooleanChromosome>(sample, POPULATION_SIZE);
		

		Fitness<BooleanChromosome> fit = new Fitness<BooleanChromosome>(1, false) {
			@Override
			public void evaluate(Individual<BooleanChromosome> individual) {
				
				BooleanChromosome chrom = individual.getChromosome();
				List<AtomicPredicate> selectedPredicates = new ArrayList<>();
				for (int i = 0; i < chrom.length(); i++) {
					int val = chrom.getValue(i) ? 1:0;
					if (chrom.getValue(i))
						selectedPredicates.add(WorkloadPredicates.predicates[i]);
				}
				
				double totalCost = 0.0;
				if(selectedPredicates.isEmpty()) {
					individual.setScore(totalCost);
					individual.setLegal(false);
					return;
				}
				
				System.out.println("SELECTED PREDICATES: " + selectedPredicates);
				
				Map<SimpleTable, Set<AtomicPredicate>> partitionedSchema = null;
				try {
					PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS partitioned CASCADE;");
					PostgresDBService.executeUpdate("VACUUM;");
					partitionedSchema = createNewPartitionedSchema("public", "partitioned", m_tables, selectedPredicates);					
				} catch (FileNotFoundException | SQLException
						| ClassNotFoundException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException
						| InstantiationException | JSONException e) {
					System.err.println("The solution could not be evaluated. Stopping genetic algorithm.");
					e.printStackTrace();
					System.exit(1);
				}

				String[] queries = mk.ukim.finki.postgresql.Constants.WORKLOAD.split(";");
				for (int i = 0; i < queries.length; i++) {
					WorkloadQuery q = new WorkloadQuery(queries[i]);					
					try {
						q.extractPredicates();
						//System.out.println(q.getSql());
						totalCost += q.calculateExecutionCost(partitionedSchema, true);
					} catch (JSONException | SQLException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}
				
				//System.out.println("Total preds: " + WorkloadPredicates.predicates.length);
				//System.out.println("Selected preds: " + selectedPredicates.size());
				System.out.println("CHROMOSOME COST = " + totalCost);
				//System.out.println();
				
				// We do not allow an execution cost of 0.0
				
				individual.setScore(totalCost);
				if(totalCost == 0.0) {
					individual.setLegal(false);
				} else {
					individual.setLegal(true);
					if(individual.getScore() < globalMin)
						globalMin = individual.getScore();
				}
				
			
				
				
				
				
				// Re-create the partitioned schema, but this time insert real data into partitions
				
				/*try {
					partitionedSchema = createEmptyPartitionedSchema("public", "partitioned", m_tables, selectedPredicates);
					insertDataIntoPartitionedSchema("public", "partitioned", partitionedSchema);
					PostgresDBService.executeUpdate("VACUUM;");
					PostgresDBService.executeUpdate("ANALYZE;");
				} catch (FileNotFoundException | SQLException e) {
					e.printStackTrace();
				}
								
				totalCost = 0.0;
				for (int i = 0; i < queries.length; i++) {
					WorkloadQuery q = new WorkloadQuery(queries[i]);					
					try {
						q.extractPredicates();
						System.out.println(q.getSql());
						totalCost += q.calculateExecutionCost(partitionedSchema, false);
					} catch (JSONException | SQLException e){
						System.out.println("Error in actual data evaluation!");
						e.printStackTrace();
						System.exit(1);
					}
				}
				estimatedToActual.put(individual.getChromosome().toString(), totalCost);
				
				//System.out.println("(DATA) Total preds: " + WorkloadPredicates.predicates.length);
				//System.out.println("(DATA) Selected preds: " + selectedPredicates.size());
				System.out.println("(DATA) CHROMOSOME COST = " + totalCost);
				//System.out.println();	
				*/				
			}			
		};
		
		GenerationEventListener<BooleanChromosome> generationEventListener = new GenerationEventListener<BooleanChromosome>() {
			@Override
			public void onGeneration(GeneticAlgorithm ga, long time) {
				Statistics stat = ga.getLastPopulation().getStatistics();
				int generation = ga.getGeneration();
				Group legals = stat.getGroup(Population.LEGALS);
				double min_score = legals.getMin()[0];
				double mean_score = legals.getMean()[0];
				//double actual_cost = estimatedToActual.get(stat.getLegalLowestIndividual().getChromosome().toString());
				/*double actualCost = estimatedToActual.get(fittestIndividualForGen)*/
				try(PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter(f, true)))) {
					p.println(generation + "," + min_score + "," + mean_score + "," + globalMin/* + "," + actual_cost*/);
					p.close();
				} catch (IOException e) {
					System.err.println("Could not write to results file!");
					e.printStackTrace();
				}
			}
		};

		GeneticAlgorithm<BooleanChromosome> ga = new GeneticAlgorithm<BooleanChromosome>(fit, pop, GENERATION_LIMIT);
	
		AbstractStage<BooleanChromosome> selection = new TournamentSelector<BooleanChromosome>(3);
		AbstractStage<BooleanChromosome> crossover = new OnePointCrossover<BooleanChromosome>(0.8);
		AbstractStage<BooleanChromosome> mutation = new SimpleMutator<BooleanChromosome>(0.02);
		ga.addStage(selection);
		ga.addStage(crossover);
		ga.addStage(mutation);		
		ga.addGenerationEventListener(generationEventListener);

		ga.setElitism(1); 	

		ga.evolve();

		Population.Statistics stats = ga.getLastPopulation().getStatistics();
		GeneticAlgorithm.Statistics algostats = ga.getStatistics();

		System.out.println("Objective: " + (fit.getBiggerIsBetter()[0] ? "Max! (All true)" : "Min! (none true)"));
		System.out.println();

		Group legals = stats.getGroup(Population.LEGALS);
		Individual solution = legals.get(0);

		System.out.println("Solution: ");
		System.out.println(solution);
		System.out.format("found in %d ms.\n", algostats.getExecutionTime());
		System.out.println();
		
		try(PrintWriter pp = new PrintWriter(new BufferedWriter(new FileWriter(f, true)))) {
			pp.println();
			pp.println("Lowest score found with evolution up to last generation:");
			pp.println(solution);
			pp.println("Score: " + solution.getScore());
			pp.close();
		}

		Utils.printStatistics(stats);
		
	

	}	
}

package jenes.implementation;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException, JSONException, IOException {
		JenesGA jenesGA = new JenesGA();
		jenesGA.setAllPredicatesArray();
		jenesGA.initConnection("postgres", "postgres", "postgres");
		jenesGA.runga();
		jenesGA.closeConnection();
	}
}

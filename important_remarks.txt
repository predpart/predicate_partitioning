-- 10.02.2015 --
1. Postgresql.conf
	a) constraint_exclusion = on;
	b) default_statistics_target = 5,000;
	(da se proba so povisok default_statistics_target)
	
	* so a) i b) povtorno se dobiva plan rows = 1; (ne sekogas)
	
2. SystemCatalogManager.java
	STAINHERIT: If true, the stats include inheritance child columns , not just the values in the specified relation
	Statistikite na particiite se azuriraat samo kaj stainherited = false, sto e edinstveniot zapis koj postoi. Kaj master tabelite, stainherited = true, sto 
	zanci deka se cuvaat informacii i za kolonite na decata nadolu vo hierarhijata na nasleduvanje, pa ottamu i kopiranje na kompletnite statistiki od
	neparticioniranata schema.
	
	STADISTINCT: Pred bilo kakvi promeni prezentirani podolu, vo momentot stadistinct se kopira od soodvetnata neparticionirana relacija. Ova ociglendo ne e vo red.
	
	
ZAVRSENO!!

-- 14.02.2015 --
Da se proveri dali ima potreba azuriranjeto na histogrami, mcv, mcf itn... treba da gi zeme vo predvid i ostanatite predikati za particijata koi ne se
odnesuvaat na atributot cii sto statistiki se azuriraat.